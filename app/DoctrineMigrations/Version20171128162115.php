<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Mangopay to stripe
 */
class Version20171128162115 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE command ADD stripe_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command ADD stripe_pre_auth_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command DROP mangopay_id');
        $this->addSql('ALTER TABLE command DROP mango_pay_pre_auth_id');
        $this->addSql('ALTER TABLE framasite_user RENAME COLUMN mangopay_id TO stripe_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE command ADD mangopay_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command ADD mango_pay_pre_auth_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command DROP stripe_id');
        $this->addSql('ALTER TABLE command DROP stripe_pre_auth_id');
        $this->addSql('ALTER TABLE framasite_user RENAME COLUMN stripe_id TO mangopay_id');
    }
}
