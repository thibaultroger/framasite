<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171018151137 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE command_domain_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE command_domain (id INT NOT NULL, command_id INT DEFAULT NULL, domain_id INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, total DOUBLE PRECISION NOT NULL, cut DOUBLE PRECISION NOT NULL, status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_18EAD74833E1689A ON command_domain (command_id)');
        $this->addSql('CREATE INDEX IDX_18EAD748115F0EE5 ON command_domain (domain_id)');
        $this->addSql('ALTER TABLE command_domain ADD CONSTRAINT FK_18EAD74833E1689A FOREIGN KEY (command_id) REFERENCES command (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command_domain ADD CONSTRAINT FK_18EAD748115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command DROP CONSTRAINT fk_8ecaead4115f0ee5');
        $this->addSql('DROP INDEX idx_8ecaead4115f0ee5');
        $this->addSql('ALTER TABLE command DROP domain_id');
        $this->addSql('ALTER TABLE command RENAME COLUMN domain_name TO domain_names');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE command_domain_id_seq CASCADE');
        $this->addSql('DROP TABLE command_domain');
        $this->addSql('ALTER TABLE command ADD domain_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE command RENAME COLUMN domain_names TO domain_name');
        $this->addSql('ALTER TABLE command ADD CONSTRAINT fk_8ecaead4115f0ee5 FOREIGN KEY (domain_id) REFERENCES domain (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_8ecaead4115f0ee5 ON command (domain_id)');
        $this->addSql('ALTER TABLE framasite_user ALTER type SET DEFAULT 1');
    }
}
