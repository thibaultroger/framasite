<?php

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\FramasitesTestCase;

class DefaultControllerTest extends FramasitesTestCase
{
    /**
     * Test default homepage when not logged-in
     */
    public function testIndexUnLogged()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Framasite', $crawler->filter('h1.big-title')->text());
    }

    /**
     * Test default homepage when logged-in
     */
    public function testIndexLogged()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('index.start.create', $crawler->filter('#create_new_website')->text());
    }

    /**
     * Test the stats page
     */
    public function testStats()
    {
        $client = static::createClient();

        $client->request('GET', '/stats');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('content-type'));
        $this->assertContains('users', $client->getResponse()->getContent());
    }

    /**
     * Test the removal page
     */
    public function testRemoval()
    {
        $this->logInAs('empty');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings/profile');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('user.delete.title', $crawler->filter('#profile')->extract(['_text'])[0]);

        $crawler = $client->request('GET', '/bye');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=delete_account_form_save]')->form();

        $data = [
            'delete_account_form[password]' => 'mypassword',
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->followRedirect();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * Test default homepage when logged-in
     * @depends testRemoval
     */
    public function testUserRemoved()
    {
        $this->assertEmpty($this->getEntityManager()->getRepository('AppBundle:User')->findBy(['username' => 'empty']), 'If the user has correctly been deleted');
    }
}
