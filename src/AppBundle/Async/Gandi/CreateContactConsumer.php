<?php

namespace AppBundle\Async\Gandi;

use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Contact;
use AppBundle\Exception\APIException;
use AppBundle\Helper\DomainFactory;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class CreateContactConsumer implements ConsumerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * CreateContactConsumer constructor.
     * @param LoggerInterface $logger
     * @param DomainFactory $domainFactory
     * @param Serializer $serializer
     * @param ProducerInterface $producer
     */
    public function __construct(LoggerInterface $logger, DomainFactory $domainFactory, Serializer $serializer, ProducerInterface $producer)
    {
        $this->logger = $logger;
        $this->domainFactory = $domainFactory;

        $this->serializer = $serializer;
        $this->producer = $producer;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $this->logger->info('Message is', [json_decode($msg->getBody(), true)]);
        /** @var CommandDomain $commandDomain */
        $commandDomain = $this->serializer->deserialize($msg->getBody(), CommandDomain::class, 'json');
        $this->logger->info('Deserialized command domain is', [$commandDomain]);
        $contact = $commandDomain->getDomain()->getContact('owner');
        $this->logger->info('Contact is ', [$contact]);

        if (!($commandDomain instanceof CommandDomain) && ($contact instanceof Contact)) {
            $this->logger->critical('Wrong message for CreateContactConsumer');
            return true;
        }

        try {
            $this->logger->info('Creating contact');

            $createdContact = $this->domainFactory->createContact($contact, $commandDomain->getDomain());

            $this->logger->info("Created contact. Calling CheckContactConsumer to check that it's ok", [$createdContact]);
            $this->producer->publish($this->serializer->serialize($commandDomain, 'json', SerializationContext::create()->setGroups(['domain_register'])));

            return true;
        } catch (APIException $e) {
            $this->logger->error('Error while creating contact', [$e->getMessage()]);
            return false;
        }
    }
}
