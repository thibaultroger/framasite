<?php

namespace AppBundle\Exception\APIException\Release;

use AppBundle\Exception\APIException\DomainException;

class ReleaseDomainException extends DomainException
{
}
