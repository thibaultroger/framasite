<?php

namespace AppBundle\Exception\APIException;

use AppBundle\Exception\APIException;

class ContactException extends APIException
{

    /**
     * @var string
     */
    private $gandiId;

    /**
     * ContactException constructor.
     * @param string $message
     * @param string $gandiId
     */
    public function __construct(string $message, string $gandiId = '')
    {
        parent::__construct($message);
        $this->gandiId = $gandiId;
    }

    /**
     * @return string
     */
    public function getGandiId()
    {
        return $this->gandiId;
    }

    /**
     * @param mixed $gandiId
     * @return ContactException
     */
    public function setGandiId(string $gandiId): ContactException
    {
        $this->gandiId = $gandiId;
        return $this;
    }
}
