<?php

namespace AppBundle\Helper;

use AppBundle\Service\StripeKeyService;
use Psr\Log\LoggerInterface;
use Stripe\Card;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;
use Symfony\Component\Routing\RouterInterface;

class PaymentFactory
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * Legal user types
     */
    const LEGAL_USER_BUSINESS = 'BUSINESS';
    const LEGAL_USER_ORGANIZATION = 'ORGANIZATION';
    const LEGAL_USER_SOLETRADER = 'SOLETRADER';

    /**
     * PaymentFactory constructor.
     * @param LoggerInterface $logger
     * @param RouterInterface $router
     * @param StripeKeyService $stripeKeyService
     */
    public function __construct(LoggerInterface $logger, RouterInterface $router, StripeKeyService $stripeKeyService)
    {
        $this->logger = $logger;
        $this->router = $router;
        Stripe::setApiKey($stripeKeyService->getPrivateKey());
    }

    /**
     * Do a simple charge
     *
     * @param int $amount The amount of the charge in euros * 10
     * @param string $token The Stripe Token
     * @param string $description A description for us
     * @param bool $capture Whether or not to process the charge right now. Default is false because we will call captureCharge after the domain is processed
     * @return Charge
     */
    public function charge(int $amount, string $token, string $description, bool $capture = false): Charge
    {
        return Charge::create(
            [
                "amount" => $amount,
                "currency" => "eur",
                "description" => $description,
                "source" => $token,
                'capture' => $capture,
            ]
        );
    }

    /**
     * @param int $amount
     * @param string $customerId
     * @param string $description
     * @param string $cardId
     * @param bool $capture
     * @return Charge
     */
    public function chargeUser(int $amount, string $customerId, string $description, string $cardId = null, $capture = false): Charge
    {
        $params = [
            'amount' => $amount,
            'currency' => 'eur',
            'description' => $description,
            'customer' => $customerId,
            'capture' => $capture,
        ];
        if (null != $cardId) {
            $params['source'] = $cardId;
        }
        return Charge::create($params);
    }

    /**
     * @param string $chargeId
     * @return Charge
     */
    public function captureCharge(string $chargeId): Charge
    {
        $charge = Charge::retrieve($chargeId);
        return $charge->capture();
    }

    /**
     * @param string $chargeId
     * @return Charge
     */
    public function getCharge(string $chargeId): Charge
    {
        return Charge::retrieve($chargeId);
    }

    /**
     * Create a new Stripe customer
     *
     * @param string $userEmail
     * @param string $token
     * @return Customer
     */
    public function createUser(string $userEmail, string $token): Customer
    {
        return Customer::create([
            'email' => $userEmail,
            'source' => $token
                                ]);
    }

    /**
     * @param string $userId
     * @return Customer
     */
    public function getUser(string $userId): Customer
    {
        return Customer::retrieve($userId);
    }

    /**
     * @param string $userId
     * @param int $limit
     * @return array<Card>
     */
    public function listCards(string $userId, int $limit = 5): array
    {
        $customer = Customer::retrieve($userId);
        return $customer->sources->all(['limit' => $limit, 'object' => 'card'])->data;
    }

    /**
     * @param string $userId
     * @param string $token
     * @return Card
     */
    public function createCard(string $userId, string $token): Card
    {
        $customer = Customer::retrieve($userId);
        return $customer->sources->create(['source' => $token]);
    }

    /**
     * @param string $userId
     * @param string $cardId
     * @return Card
     */
    public function removeCard(string $userId, string $cardId): Card
    {
        $customer = Customer::retrieve($userId);
        return $customer->sources->retrieve($cardId)->delete();
    }
}
