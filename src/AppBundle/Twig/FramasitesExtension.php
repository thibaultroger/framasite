<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Command;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Domain;
use AppBundle\Entity\Payment\Card as CardEntity;
use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Entity\Site;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Helper\PaymentFactory;
use AppBundle\Helper\SiteFactory;
use MangoPay\Card;
use MangoPay\CardPreAuthorization;
use MangoPay\PayIn;
use Stripe\Charge;

class FramasitesExtension extends \Twig_Extension
{
    /**
     * @var SiteFactory
     */
    private $siteFactory;

    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * FramasitesExtension constructor.
     * @param SiteFactory $siteFactory
     * @param PaymentFactory $paymentFactory
     */
    public function __construct(SiteFactory $siteFactory, PaymentFactory $paymentFactory)
    {
        $this->siteFactory = $siteFactory;
        $this->paymentFactory = $paymentFactory;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('domain_status', [$this, 'domainStatus']),
            new \Twig_SimpleFilter('command_status', [$this, 'commandStatus']),
            new \Twig_SimpleFilter('command_payment_type', [$this, 'commandPaymentType']),
            new \Twig_SimpleFilter('contact_type', [$this, 'contactType']),
            new \Twig_SimpleFilter('expiration_date', [$this, 'expirationDate']),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('site_infos', [$this, 'siteInfos']),
            new \Twig_SimpleFunction('payment_details', [$this, 'paymentDetails']),
            new \Twig_SimpleFunction('payment_card_details', [$this, 'paymentCardDetails']),
        ];
    }

    /**
     * @return array
     */
    public function getTests()
    {
        return [
            new \Twig_SimpleTest('blog', function (Site $site) {
                return $site instanceof Blog;
            }),
            new \Twig_SimpleTest('single', function (Site $site) {
                return $site instanceof SinglePage;
            }),
            new \Twig_SimpleTest('wiki', function (Site $site) {
                return $site instanceof Wiki;
            }),
        ];
    }

    /**
     * @param Site $site
     * @return array
     */
    public function siteInfos(Site $site): array
    {
        return $this->siteFactory->loadInformationFromSite($site);
    }

    /**
     * @param string $chargeId
     * @return Charge
     */
    public function paymentDetails(string $chargeId): Charge
    {
        return $this->paymentFactory->getCharge($chargeId);
    }

    /**
     * @param string $cardId
     * @return Card
     */
    public function paymentCardDetails(string $cardId = null)
    {
        return $cardId != null ? $this->paymentFactory->getCard($cardId) : null;
    }

    /**
     * @param int $status
     * @return string
     */
    public function domainStatus(int $status): string
    {
        switch ($status) {
            case Domain::DOMAIN_REQUESTED:
                $trans = 'domain.status.requested';
                break;
            case Domain::DOMAIN_PAYED:
                $trans = 'domain.status.payed';
                break;
            case Domain::DOMAIN_VALIDATED:
                $trans = 'domain.status.validated';
                break;
            case Domain::DOMAIN_DNS_CHANGED:
                $trans = 'domain.status.dns_changed'; // depreciated
                break;
            case Domain::DOMAIN_WAITING_CONFIGURATION:
                $trans = 'domain.status.waiting_configuration';
                break;
            case Domain::DOMAIN_VHOST_ADDED:
                $trans = 'domain.status.vhost_added'; // depreciated
                break;
            case Domain::DOMAIN_OK:
                $trans = 'domain.status.ok';
                break;
            case Domain::DOMAIN_DETACHED:
                $trans = 'domain.status.detached';
                break;
            case Domain::DOMAIN_REMOVED:
                $trans = 'domain.status.removed'; // depreciated
                break;
            case 0:
            default:
                $trans = 'domain.status.unknown_error';
                break;
        }
        return $trans;
    }

    /**
     * @param int $commandStatus
     * @return string
     */
    public function commandStatus(int $commandStatus): string
    {
        switch ($commandStatus) {
            case Command::STATUS_CREATED:
                $trans = 'command.status.created';
                break;
            case Command::STATUS_SEND:
                $trans = 'command.status.sent';
                break;
            case Command::STATUS_RESPONDED:
                $trans = 'command.status.responded';
                break;
            case Command::STATUS_WAITING_CONFIGURATION:
                $trans = 'command.status.waiting_configuration';
                break;
            case Command::STATUS_COMMAND_CANCELLED:
                $trans = 'command.status.cancelled';
                break;
            case Command::STATUS_PAYMENT_CANCELLED:
                $trans = 'command.status.payment_cancelled';
                break;
            case Command::STATUS_PAYMENT_ISSUE:
                $trans = 'command.status.payment_issue';
                break;
            case Command::STATUS_FINISHED:
                $trans = 'command.status.finished';
                break;
            case Command::STATUS_PARTIALLY_FINISHED:
                $trans = 'command.status.partially_finished';
                break;
            default:
                $trans = 'command.status.unknown';
                break;
        }
        return $trans;
    }

    /**
     * @param int $commandPaymentType
     * @return string
     */
    public function commandPaymentType(int $commandPaymentType): string
    {
        switch ($commandPaymentType) {
            case Command::PAYMENT_TYPE_DIRECT:
                $trans = 'command.payment_type.direct';
                break;
            case Command::PAYMENT_TYPE_SAVED_CARD:
                $trans = 'command.payment_type.saved_card';
                break;
            case Command::PAYMENT_TYPE_BANKWIRE:
                $trans = 'command.payment_type.bankwire';
                break;
            default:
                $trans = 'command.payment_type.unknown';
                break;
        }
        return $trans;
    }

    /**
     * @param int $contactType
     * @return string
     */
    public function contactType(int $contactType): string
    {
        switch ($contactType) {
            case Contact::CONTACT_TYPE_OWNER:
                $trans = 'domain.contactType.owner';
                break;
            case Contact::CONTACT_TYPE_ADMIN:
                $trans = 'domain.contactType.admin';
                break;
            case Contact::CONTACT_TYPE_BILL:
                $trans = 'domain.contactType.bill';
                break;
            case Contact::CONTACT_TYPE_TECH:
                $trans = 'domain.contactType.tech';
                break;
            case Contact::CONTACT_TYPE_RESELLER:
                $trans = 'domain.contactType.reseller';
                break;
            default:
                $trans = 'domain.contactType.unknown';
                break;
        }
        return $trans;
    }

    /**
     * @param string $dateStr
     * @return \DateTime
     */
    public function expirationDate(string $dateStr): \DateTime
    {
        $dateArr = str_split($dateStr, 2);
        return new \DateTime('20' . $dateArr[1] . '-' . $dateArr[0]);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'framasites_extension';
    }
}
