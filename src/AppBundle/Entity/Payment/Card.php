<?php

namespace AppBundle\Entity\Payment;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="card")
 */
class Card
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $mangoPayId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="cards")
     */
    private $user;

    /**
     * Card constructor.
     * @param string $mangoPayId
     * @param User $user
     */
    public function __construct(string $mangoPayId, User $user)
    {
        $this->mangoPayId = $mangoPayId;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMangoPayId(): string
    {
        return $this->mangoPayId;
    }

    /**
     * @param string $mangoPayId
     * @return Card
     */
    public function setMangoPayId(string $mangoPayId): Card
    {
        $this->mangoPayId = $mangoPayId;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Card
     */
    public function setUser(User $user): Card
    {
        $this->user = $user;
        return $this;
    }
}
