<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="command_domain")
 * @ORM\HasLifecycleCallbacks()
 */
class CommandDomain
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     * @Groups({"domain_register"})
     */
    private $id;

    /**
     * @var Command
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Command", inversedBy="commandDomains", cascade={"persist"}, fetch="EAGER")
     * @Groups({"domain_register"})
     */
    private $command;

    /**
     * @var Domain
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Domain", inversedBy="commands", cascade={"persist"}, fetch="EAGER")
     * @Groups({"domain_register"})
     */
    private $domain;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Groups({"domain_register"})
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Groups({"domain_register"})
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $cut = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Groups({"domain_register"})
     */
    private $status;

    const STATUS_ERROR = 0;
    const STATUS_INITIAL = 1;
    const STATUS_TREATED = 2;
    const STATUS_CREATED = 3;
    const STATUS_PROCESSED = 4;
    const STATUS_FINISHED = 4;

    /**
     * CommandDomain constructor.
     * @param float $amount
     * @param float $total
     * @param float $cut
     */
    public function __construct(float $amount, float $total, float $cut = 0)
    {
        $this->status = self::STATUS_INITIAL;
        $this->amount = $amount;
        $this->total = $total;
        $this->cut = $cut;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CommandDomain
     */
    public function setId(int $id): CommandDomain
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Command
     */
    public function getCommand(): Command
    {
        return $this->command;
    }

    /**
     * @param Command $command
     * @return CommandDomain
     */
    public function setCommand(Command $command): CommandDomain
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return Domain
     */
    public function getDomain(): Domain
    {
        return $this->domain;
    }

    /**
     * @param Domain $domain
     * @return CommandDomain
     */
    public function setDomain(Domain $domain): CommandDomain
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return CommandDomain
     */
    public function setStatus(int $status): CommandDomain
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return CommandDomain
     */
    public function setAmount(float $amount): CommandDomain
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return CommandDomain
     */
    public function setTotal(float $total): CommandDomain
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return float
     */
    public function getCut(): float
    {
        return $this->cut;
    }

    /**
     * @param float $cut
     * @return CommandDomain
     */
    public function setCut(float $cut): CommandDomain
    {
        $this->cut = $cut;
        return $this;
    }
}
