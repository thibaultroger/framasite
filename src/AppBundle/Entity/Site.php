<?php

namespace AppBundle\Entity;

use AppBundle\Entity\SinglePage\SinglePage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as FramasitesAssert;

/**
 * /**
 * @ORM\MappedSuperclass
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SiteRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({1 = "AppBundle\Entity\Blog\Blog", 2 = "AppBundle\Entity\Wiki\Wiki", 3 = "AppBundle\Entity\SinglePage\SinglePage"})
 * @ORM\HasLifecycleCallbacks()
 * @FramasitesAssert\ConstraintValidSiteConfiguration()
 */
abstract class Site
{
    const TYPE_UNKNOWN = 0;
    const TYPE_GRAV = 1;
    const TYPE_DOKUWIKI = 2;
    const TYPE_PRETTY_NOEMIE = 3;

    const FORBIDDEN_SUBDOMAINS = ['www', 'hostmaster', 'postmaster', 'webmaster', 'admin', 'contact', 'soutenir', 'framasoft', 'asso', 'site', 'root', 'frama', 'degooglisons', 'contributopia'];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subdomain", type="string")
     * @Assert\NotBlank()
     * @Assert\Length(max=25, min=2)
     * @Assert\Regex("/^[a-z0-9][-a-z0-9]{0,23}[a-z0-9]$/")
     */
    protected $subdomain;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     */
    protected $createdAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="sites")
     * @Assert\NotBlank()
     */
    protected $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Domain", mappedBy="site")
     */
    protected $domains;

    /**
     * @var array
     */
    protected $siteUsers;

    /**
     * Site constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
        $this->domains = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * @param string $subdomain
     * @return Site
     */
    public function setSubdomain(string $subdomain): Site
    {
        $this->subdomain = $subdomain;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Site
     */
    public function setCreatedAt(\DateTime $createdAt): Site
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Site
     */
    public function setUser(User $user): Site
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDomains()
    {
        return $this->domains;
    }

    /**
     * @param ArrayCollection $domains
     * @return Site
     */
    public function setDomains(ArrayCollection $domains): Site
    {
        $this->domains = $domains;
        return $this;
    }

    /**
     * @param Domain $domain
     * @return Site
     */
    public function addDomain(Domain $domain): Site
    {
        $this->domains->add($domain);
        return $this;
    }

    /**
     * @param Domain $domain
     * @return bool
     */
    public function hasDomain(Domain $domain): bool
    {
        return $this->domains->contains($domain);
    }

    public function __toString()
    {
        return $this->subdomain . '.frama.xxx';
    }

    /**
     * @return array
     */
    public function getSiteUsers()
    {
        return $this->siteUsers;
    }

    /**
     * @param $siteUsers
     * @return Site
     */
    public function setSiteUsers($siteUsers): Site
    {
        $this->siteUsers = $siteUsers;
        return $this;
    }

    /**
     * @return AbstractSiteUser|array|mixed|null
     */
    public function getAdmin()
    {
        if ($this instanceof SinglePage) {
            return $this->siteUsers[0];
        }
        if (is_array($this->siteUsers)) {
            foreach ($this->siteUsers as $siteUser) {
                /** @var $siteUser AbstractSiteUser */
                if ($siteUser->isAdmin()) {
                    return $siteUser;
                }
            }
            return null;
        }
        return $this->siteUsers;
    }
}
