<?php

namespace AppBundle\Service\Command;

use AppBundle\Entity\Command;
use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Domain;
use AppBundle\Entity\User;
use AppBundle\Exception\PaymentException;
use AppBundle\Helper\DomainFactory;
use AppBundle\Helper\PaymentFactory;
use AppBundle\Service\BaseService;
use AppBundle\Service\Email\InvoiceService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CommandService extends BaseService
{

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * CommandService constructor.
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     * @param DomainFactory $domainFactory
     * @param PaymentFactory $paymentFactory
     * @param InvoiceService $invoiceService
     */
    public function __construct(
        LoggerInterface $logger,
                                EntityManagerInterface $entityManager,
                                DomainFactory $domainFactory,
                                PaymentFactory $paymentFactory,
                                InvoiceService $invoiceService
    ) {
        parent::__construct($logger, $entityManager);
        $this->entityManager = $entityManager;
        $this->domainFactory = $domainFactory;
        $this->paymentFactory = $paymentFactory;
        $this->invoiceService = $invoiceService;
    }

    /**
     * @param Domain $domain
     * @param User $user
     * @param float $marge
     * @return Command
     * @throws \AppBundle\Exception\APIException
     */
    public function newCommand(Domain $domain, User $user, float $marge = 1): Command
    {
        $amount = $this->domainFactory->calculatePriceForAction($domain->getDomainName(), 1, 'domains', 'create');

        $margeAmount = round($amount * $marge, 2);
        $total = $amount + $margeAmount;

        /**
         * If we have already an active command
         */
        $command = $user->getActiveCommand();
        if (null === $command) {
            $this->logger->info("Creating a new command");
            $command = new Command();
            $command->setUser($user);
            $this->entityManager->persist($command);
            $this->entityManager->persist($user);
        }

        /**
         * Adding the domain to the command
         */
        $commandDomain = new CommandDomain($amount, $total);
        $commandDomain->setCommand($command)->setDomain($domain);
        $this->entityManager->persist($commandDomain);

        $this->logger->info("Adding domain " . $domain->getDomainName() . " to command #" . $command->getId());
        $command->addCommandDomain($commandDomain);

        $user->setActiveCommand($command);

        $this->entityManager->persist($command);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $command;
    }

    /**
     * Make payment if we have to handle them after BuyDomains finishes
     *
     * @param Command $command
     * @param User $user
     */
    public function makePayment(Command $command, User $user)
    {
        $this->logger->info('Capturing payment charge for command n°' . $command->getId() . ' by user ' . $user->getUsername());
        $paymentId = $command->getStripeId();

        $this->paymentFactory->captureCharge($paymentId);

        $this->logger->info('Payment charge effective');

        $command->setStatus(Command::STATUS_FINISHED);
        $this->entityManager->persist($command);
        $this->logger->info('Command is finished');

        $this->invoiceService->sendInvoice($command);
    }
}
