<?php

namespace AppBundle\Form\Wiki;

use AppBundle\Entity\Wiki\WikiUser;
use AppBundle\Form\AbstractUserType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class WikiUserType
 * @package AppBundle\Form\Wiki
 */
class WikiUserType extends AbstractUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'site.new.user.create',
                'attr' => ['class' => 'btn-success'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => WikiUser::class,
            'user' => null,
       ]);
    }
}
