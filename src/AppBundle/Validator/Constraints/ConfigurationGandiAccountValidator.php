<?php

namespace AppBundle\Validator\Constraints;

use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Narno\Gandi\Api as GandiAPI;

class ConfigurationGandiAccountValidator extends ConstraintValidator
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var GandiAPI
     */
    private $api;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ConfigurationDomainValidator constructor.
     * @param LoggerInterface $logger
     * @param $apiMode
     * @param $apiKeyProd
     * @param $apiKeyTest
     */
    public function __construct(LoggerInterface $logger, $apiMode, $apiKeyProd, $apiKeyTest)
    {
        $this->logger = $logger;
        try {
            $this->api = new GandiAPI($apiMode !== 'prod');
            // set API key
            $this->apiKey = $apiKeyTest;
            if ($apiMode === 'prod') {
                $this->apiKey = $apiKeyProd;
            }
        } catch (\RuntimeException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string $gandiId
     * @param Constraint $constraint
     */
    public function validate($gandiId, Constraint $constraint)
    {
        try {
            $this->api->contact->info([$this->apiKey, $gandiId]);
        } catch (\RuntimeException $e) {
            if (strpos($e->getMessage(), 'CAUSE_BADPARAMETER') !== false) {
                $this->context->buildViolation($constraint->incorrectGandiAccountMessage)
                    ->addViolation();
            } elseif (strpos($e->getMessage(), 'CAUSE_NOTFOUND') !== false) {
                $this->context->buildViolation($constraint->notFoundGandiAccountMessage)
                    ->addViolation();
            } else {
                // Unknown exception
                $this->logger->error($e->getMessage());
            }
        }
    }
}
