<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintValidGandiAccountConfiguration extends Constraint
{
    public $incorrectGandiAccountMessage = 'gandi.account.incorrect';
    public $notFoundGandiAccountMessage = 'gandi.account.not_found';

    public function validatedBy()
    {
        return ConfigurationGandiAccountValidator::class;
    }
}
