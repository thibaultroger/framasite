<?php

namespace AppBundle\Event\Listener;

use AppBundle\Exception\APIException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Twig_Environment;

class APIExceptionListener
{
    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Twig_Environment $twig, LoggerInterface $logger)
    {
        $this->twig = $twig;
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof APIException) {
            return;
        }

        $response = new Response($this->twig->render('TwigBundle:Exception:api_exception.html.twig', ['message' => $exception->getMessage()]));

        $event->setResponse($response);
    }
}
