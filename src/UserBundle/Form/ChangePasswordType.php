<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('old_password', PasswordType::class, [
                'constraints' => new UserPassword(['message' => 'validator.password_wrong_value']),
                'label' => 'settings.password.old_password_label',
                'label_attr' => ['class' => 'col-sm-4'],
            ])
            ->add('new_password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'validator.password_must_match',
                'required' => true,
                'first_options' => ['label' => 'settings.password.new_password_label', 'label_attr' => ['class' => 'col-sm-4']],
                'second_options' => ['label' => 'settings.password.repeat_new_password_label', 'label_attr' => ['class' => 'col-sm-4']],
                'constraints' => [
                    new Constraints\Length([
                       'min' => 8,
                       'minMessage' => 'settings.password.too_short',
                   ]),
                    new Constraints\NotBlank(),
                ],
                'label' => 'settings.password.new_password_label',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'settings.save',
            ])
        ;
    }

    public function getBlockPrefix()
    {
        return 'change_passwd';
    }
}
